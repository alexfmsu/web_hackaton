package io.rybalkinsd.kotlinbootcamp.server

import io.rybalkinsd.kotlinbootcamp.util.logger
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import java.util.Queue
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue
import org.springframework.http.HttpStatus
import java.text.SimpleDateFormat
import java.util.*

@Controller
@RequestMapping("/chat")
class ChatController {
    val log = logger()
    val messages: Queue<String> = ConcurrentLinkedQueue()
    val usersOnline: MutableMap<String, String> = ConcurrentHashMap()

    @RequestMapping(
            path = ["/login"],
            method = [RequestMethod.POST],
            consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE]
    )
    fun login(@RequestParam("name") name: String): ResponseEntity<String> = when {
        name.isEmpty() -> ResponseEntity.badRequest().body("Name is too short")
        name.length > 20 -> ResponseEntity.badRequest().body("Name is too long")
        usersOnline.contains(name) -> ResponseEntity.badRequest().body("Already logged in")
        else -> {
            usersOnline[name] = name
            messages += "[$name] logged in".also { log.info(it) }
            ResponseEntity.ok().build()
        }
    }

    @RequestMapping(
            path = ["/logout"],
            method = [RequestMethod.POST],
            consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE]
    )
    fun logout(@RequestParam("name") name: String): ResponseEntity<String> = when {
        // name.isEmpty() -> ResponseEntity.badRequest().body("Name is too short")
        // name.length > 20 -> ResponseEntity.badRequest().body("Name is too long")
        !usersOnline.contains(name) -> ResponseEntity.badRequest().body("User not exist")
        else -> {
            usersOnline.remove(name)
            // usersOnline[name] = name
            messages += "[$name] logged out".also { log.info(it) }
            ResponseEntity.ok().build()
        }

        // messages += "[$name] logged out".also { log.info(it) }

        // return ResponseEntity.ok().build()
    }

    /**
     *
     * Well formatted sorted list of online users
     *
     * curl -i localhost:8080/chat/online
     */
    @RequestMapping(
            path = ["online"],
            method = [RequestMethod.GET],
            produces = [MediaType.TEXT_PLAIN_VALUE]
    )
    fun online() : ResponseEntity<String> {
        var users_online: String = ""

        usersOnline.forEach { 
            key, value -> users_online = users_online.plus(key.toString() + "\n") 
        }

        if(users_online == ""){
            users_online = "No users online"
        }
        
        return ResponseEntity<String>(users_online, HttpStatus.OK)
   }

   @RequestMapping(
            path = ["say"],
            method = [RequestMethod.POST],
            consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE]
    )
    fun say(@RequestParam("name") name: String, @RequestParam("msg") msg: String): ResponseEntity<String> = when {
        !usersOnline.contains(name) -> ResponseEntity(HttpStatus.FORBIDDEN)
        msg.length > 140 -> ResponseEntity.badRequest().build()
        else -> {
            messages += "[$name] ${SimpleDateFormat("hh:mm").format(Date())} $msg".also{ log.info(it) }
            ResponseEntity.ok().build()
        }
    }

    /**
     * curl -X POST -i localhost:8080/chat/logout -d "name=MY_NAME"
     */
    // TODO

    /**
     * curl -X POST -i localhost:8080/chat/say -d "name=MY_NAME&msg=Hello everyone in this chat"
     */
    // TODO

    /**
     * curl -i localhost:8080/chat/history
     */
    @RequestMapping(
            path = ["history"],
            method = [RequestMethod.GET],
            produces = [MediaType.TEXT_PLAIN_VALUE]
    )
    @ResponseBody
    fun history(): String = messages.joinToString(separator = "\n")
}
